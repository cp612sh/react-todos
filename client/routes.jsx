var {
    Route,
    NotFoundRoute,
    DefaultRoute
    } = ReactRouter;

var routes = (
    <Route name="root" handler={ AppBody } path="/">
        <Route name="todoList" handler={ ListShow } path="/lists/:listId"/>
        <Route name="join" handler={ AuthJoinPage } path="/join"/>
        <Route name="signin" handler={ AuthSigninPage } path="/signin"/>
        <DefaultRoute handler={ AppLoading }/>
        <NotFoundRoute handler={ AppNotFound }/>
    </Route>
);

var router = ReactRouter.create({
    routes: routes,
    location: ReactRouter.HistoryLocation,
});

var showFistList = function () {
    router.replaceWith("todoList", {listId: Lists.findOne()._id});
};